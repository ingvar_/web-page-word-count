require 'test_helper'

module Project
  class ProjectTest < Minitest::Test
    def test_that_it_has_a_version_number
      refute_nil ::Project::VERSION
    end

    def test_that_scraper_can_be_initiated
      assert_instance_of Scraper, Scraper.new('http://www.delfi.ee')
    end

    def test_that_a_faulty_url_returns_socketerror
      assert_raises SocketError do
        scraper = Scraper.new 'http://www.delffi.ee'
        scraper.read_http
      end
    end

    def test_that_a_file_can_be_opened_for_writing
      f = File.open(Dir.pwd + '/' + 'test.txt', 'w')
      f.close
      assert_instance_of File, f
      File.delete('test.txt')
    end

  end
end
