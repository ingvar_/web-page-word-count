# Project

This Ruby gem named 'Project' :) will provide you the tools for:
	1) scraping a web page - the output will be a .txt or .csv file containing all the phrases/sentences (the corpus) contained in the HTML body, which can be passed on to natural language processing tools.
	2) analysing word frequencies on a web page - the output will be a .csv file including the occurrences of each word on a provided URI in descending order.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'project'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install project

## Usage

To run the scraper application, open a console and write

    $ bundle exec ruby exe/application.rb

You will be prompted for a web page URI. Then, you can choose to print the output of the scraper to your console. After that, you will be given a choice to write the output to either a .txt or .csv file. Finally, you can choose to write the word frequencies to a .csv file.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/ingvar_/ruby_project. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

