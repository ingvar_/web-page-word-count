require 'project/version'

class Application
  require 'project/web_scraper'
  require 'project/utilities'

  url_ok = false
  until url_ok
    puts 'Provide URL to start scraping:'
    print 'http(s)://'
    url_link = gets
    url_full = 'http://' + url_link
    url_full = url_full.chomp
    # a = Scraper.new "https://en.wikipedia.org/wiki/Pierre-Simon_Laplace"
    # a = Scraper.new "http://www.delfi.ee"

    scraper = Scraper.new url_full
    begin
      http_req = scraper.read_http
      url_ok = true
    rescue SocketError
      puts "Faulty URL! Provide a correct URL. \n"
    end

  end

  page_html = scraper.parse_html(http_req)
  body_html = scraper.get_body(page_html)
  sentences = scraper.split_strings_at_fstop(body_html)
  filtered_sntces = scraper.string_filtering(sentences)

  puts 'Show full output in console? (y/n):'
  choice = gets
  choice = choice.strip

  if choice.casecmp('y').zero?
    puts filtered_sntces
    puts "\n\n"
  end

  utilities = Utilities.new
  puts 'Print corpus to txt file? (y/n)'
  choice = gets
  choice = choice.strip

  if choice.casecmp('y').zero?
    fname1 = utilities.generate_corpus_filename(true)
    utilities.write_to_txt(fname1, filtered_sntces)
    puts "\n"
    puts 'File ' + fname1 + ' written.'
    puts "\n"
  end

  puts 'Print corpus to csv file? (y/n)'
  choice = gets
  choice = choice.strip

  if choice.casecmp('y').zero?
    fname2 = utilities.generate_corpus_filename(false)
    utilities.write_corpus_to_csv(fname2, filtered_sntces)
    puts "\n"
    puts 'File ' + fname2 + ' written.'
    puts "\n"
  end

  puts 'Generate word frequency table? It will be written to a csv. (y/n)'
  choice = gets
  choice = choice.strip

  if choice.casecmp('y').zero?
    wordfreqs = scraper.generate_word_frequency_hash(body_html)
    fname3 = utilities.generate_wordfreq_filename
    utilities.write_wordfreqs_to_csv(fname3, wordfreqs)
    puts "\n"
    puts 'File ' + fname3 + ' written.'
    puts "\n"
  end
end
