class Utilities
  def generate_corpus_filename(txt_file = true)
    if txt_file == true
      'Scraper_' + Time.now.strftime('%d-%m-%Y-%H:%M') + '.txt'
    else
      'Scraper_' + Time.now.strftime('%d-%m-%Y-%H:%M') + '.csv'
    end
  end

  def generate_wordfreq_filename
    'Wordfreq_' + Time.now.strftime('%d-%m-%Y-%H:%M') + '.csv'
  end

  def write_to_txt(fname, filtered_sentence_list)
    f = File.open(Dir.pwd + '/' + fname, 'w')

    filtered_sentence_list.each do |sentence|
      f.write(sentence)
      f.write("\n")
    end
  rescue IOError => e
    puts e.backtrace
  ensure
    f.close unless f.nil?
  end

  def write_corpus_to_csv(fname, filtered_sentence_list)
    f = File.open(Dir.pwd + '/' + fname, 'w')

    filtered_sentence_list.each do |sentence|
      f.write(sentence)
      f.write(',')
      f.write("\n")
    end
  rescue IOError => e
    puts e.backtrace
  ensure
    f.close unless f.nil?

    if File.zero?(Dir.pwd + '/' + fname)
      puts 'Something went wrong with writing the file.'
    end
  end

  def write_wordfreqs_to_csv(fname, word_frequency_hash)
    f = File.open(Dir.pwd + '/' + fname, 'w')

    word_frequency_hash.each do |key, value|
      f.write(key)
      f.write(',')
      f.write(value)
      f.write("\n")
    end
  rescue IOError => e
    puts e.backtrace
  ensure
    f.close unless f.nil?

    if File.zero?(Dir.pwd + '/' + fname)
      puts 'Something went wrong with writing the file.'
    end
  end
end
