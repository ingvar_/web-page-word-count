require 'rubygems'
require 'httparty'
require 'nokogiri'

class Scraper
  def initialize(input)
    @input_url = input
  end

  def read_http
    HTTParty.get(@input_url)
  end

  def parse_html(page)
    Nokogiri::HTML(page)
  end

  def get_body(page_parsed)
    # bodyHTML = page_parsed.css('body')
    page_parsed.at('body').children.text
  end

  # depth 0 - only from current page
  def get_links(page_parsed, _depth = 0)
    # TODO: depths
    links = page_parsed.css('a')
    urls = links.map { |elem| elem['href'] }.compact

    urls.each do |elem|
      if !elem.start_with?('http' || 'https') ||
         elem.end_with?('jpg' || 'png' || 'pdf')
        elem.gsub!(elem, '')
      end
    end

    urls.reject!(&:empty?)
  end

  def split_strings_at_fstop(body_html)
    body_html.split('.')
  end

  def string_filtering(split_strings_array)
    split_strings_array.each do |elem|
      # remove whitespace (but not regular spaces)
      elem.gsub!(/\s{2,}/, ' ')
      # delete sentence if it contains only one word (doesn't include a space)
      elem.gsub!(elem, '') unless elem.include? ' '
      # remove too short "sentences" (less than 20 characters, as they are useless in terms of NLP)
      elem.gsub!(elem, '') if elem.length < 25
      # remove all elements which include the newline character. although this might remove also some of the useful stuff,
      # it most certainly gets rid of the useless stuff (menu elements, tags etc)
      # elem.gsub!(elem,"") if elem.include? "\n"
      elem.split("\n")
      # remove javascript code
      elem.gsub!(elem, '') if elem.include?('{' || '}')
      # remove if sentence starts with a number (most likely irrelevant "sentence")
      elem.gsub!(elem, '') if (0..9).map(&:to_s).include? elem[0]
      # remove more code:
      elem.gsub!(elem, '') if elem.include?('php' || 'html')
      # strip and chomp in case there are singular empty spaces left that should be deleted below
      elem.strip!
    end
    split_strings_array.reject!(&:empty?)
    # split_strings_array.reject!{ |elem| elem == ' '}
  end

  def generate_word_frequency_hash(body_html)
    all_words = body_html.downcase.split(' ')
    # generates filtered word array:
    all_words.each do |elem|
      # remove javascript code
      elem.gsub!(elem, '') if elem.include? ('{' || '}')
      # remove some more code:
      elem.gsub!(elem, '') if elem.include? ('php' || 'html')
      # remove punctuation:
      elem.gsub!(/[.[\n],();?!:^"]/, ' ')
      # strip whitespaces left
      elem.strip!
    end

    all_words.reject!(&:empty?)

    frequency_hash = all_words.each_with_object({}) do |word, output|
      if output.key?(word)
        output[word] += 1
      else
        output[word] = 1
      end
    end

    # remove all numbers:
    frequency_hash = frequency_hash.reject { |key, _value| key =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/ }
    # remove all singular characters:
    frequency_hash = frequency_hash.reject { |key, _value| key.length < 2 }
    # sort the table into descending order:
    frequency_hash.sort_by { |_key, value| value }.reverse
  end
end

# require 'net/http'
# Net::HTTP.get(URI('http://www.delfi.ee'))
